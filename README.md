# EBI Basic Web Components

This package contains the main EBI header and footer components for use in Vue.js apps.

The npm package is here https://www.npmjs.com/package/@chembl/ebi-basic-components

## How to use

You can se an example for a Vue.js app here:

https://codesandbox.io/s/ebi-basic-components-vue-7dj84

And a example for a Nuxt.js app here:

https://codesandbox.io/s/ebi-basic-components-nuxt-ttd0t

## Project setup

```bash
npm install
```

### Compiles and hot-reloads for development

```bash
vue serve ./src/lib-dev.vue
```

### Builds bundle for publishing

```bash
npm run build
```

### Lints files (doesn't fix them)

```bash
npm run lint
```

### Lints and fixes files

```bash
npm run lint-fix
```
