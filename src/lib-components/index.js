import EBIHeader from './EBIHeader.vue';
import EBIFooter from './EBIFooter.vue';

export { EBIHeader, EBIFooter };